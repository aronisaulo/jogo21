package com.company;

import java.util.Scanner;

public class Rodada {

    public void jogar() {
        Cartas cartas = new Cartas();
        int valorjogo = 0;
        boolean pedircarta = true;

        while (pedircarta) {
            valorjogo += cartas.sortear();
            System.out.println("Pontos: "+valorjogo);

            if (valorjogo >= 21) {
                pedircarta = false;
                continue;
            }

            System.out.println("Pedir Carta S/N ?");
            Scanner sc = new Scanner(System.in);
            String sair = sc.next();

            if (sair.toUpperCase().equals("N") ) {
                pedircarta = false;
             }

        }

        if (valorjogo > 21)
            System.out.println("Perdeu :( ");
        else if (valorjogo == 21)   System.out.println(" ******** Ganhou ***********");
        else System.out.println("Desistiu Mano ?");

    }

}
