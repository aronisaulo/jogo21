package com.company;

import java.util.HashMap;
import java.util.Random;
import java.util.TreeMap;

public class Cartas {

    private TreeMap<Integer,String> rodada = new TreeMap<Integer, String>();

    public TreeMap<Integer, String> getRodada() {
        return rodada;
    }

    public void setRodada(TreeMap<Integer, String> rodada) {
        this.rodada = rodada;
    }

    public int sortear(){
            Random rd = new Random();
            int cartaValor = rd.nextInt(Baralho.values().length);
            int cartaNipe  = rd.nextInt(Nipes.values().length);

            int index = rodada.size()  ;
            rodada.put(index, Nipes.values()[cartaNipe].toString() + " | " + Baralho.values()[cartaValor].toString() )  ;
            System.out.print(" ( " + rodada.get(index)+ " ) ");

            return Baralho.values()[cartaValor].getValor();
    }



}
